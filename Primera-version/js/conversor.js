// conversion ars -> usd
const ARS_USD = 1 / 140;      // 1 ars = 1/140 usd

// conversion btc -> usd
const BTC_USD = 56801.00;   // 1 btc = 56801.00 usd

// conversion eth -> usd
const ETH_USD = 1790.76;    // 1 eth = 1790.76 usd

// conversiones desde usd,ars,eth,btc a btc
const toBtc = {
    usd: 1 / BTC_USD,
    ars: ARS_USD / BTC_USD,
    eth: ETH_USD / BTC_USD,
    btc: 1
}

// conversiones desde usd,ars,eth,btc a eth
const toEth = {
    usd: 1 / ETH_USD,
    ars: ARS_USD / ETH_USD,
    eth: 1,
    btc: BTC_USD / ETH_USD
}

// conversiones desde usd,ars,eth,btc a ars
const toArs = {
    usd: 1 / ARS_USD,
    ars: 1,
    eth: ETH_USD / ARS_USD,
    btc: BTC_USD / ARS_USD
}

// conversiones desde usd,ars,eth,btc a usd
const toUsd = {
    usd: 1,
    ars: ARS_USD,
    eth: ETH_USD,
    btc: BTC_USD
}

// mapea las conversiones en general
const conversiones = {
    usd: toUsd,
    ars: toArs,
    btc: toBtc,
    eth: toEth
};



for (let coinName in conversiones) {
    document.getElementById("coin").innerHTML += `
        <div class="col-sm-6 col-md-3 col-lg-4 form-group">
            <div  class="card">
            <div class="card-body" id="${coinName}" >
                <div class="card-title">
                    <h5>Valor de ${coinName}</h5>
                </div>
                </div>
            </div>
        </div>`
}

for (let coinName in conversiones) {
    for (let coinToCoin in conversiones[coinName]) {
        function coinValueFunction(x) {
            return Number.parseFloat(x).toFixed(4)
        }
        var coinValue = conversiones[coinName][coinToCoin];

        document.getElementById(coinName).innerHTML += `
        <p><span class="text-muted fw-bold">${coinToCoin}:</span> ${coinValueFunction(coinValue)}</p>
        `
    }
}


const mensajeOperacion = {
    comprar: {
        tipoOperacion: ". Voy a pagar con",
        resultado: (cantidad, conversion, monedaOrigen, monedaDestino) => (
            `Necesitas ${conversion} <strong>${monedaDestino.toUpperCase()}</strong> para comprar ${cantidad} <strong>${monedaOrigen.toUpperCase()}</strong>`
        ),
        boton: "Comprar"
    },
    vender: {
        tipoOperacion: "Y obtener",
        resultado: (cantidad, conversion, monedaOrigen, monedaDestino) => (
            `Obtendrás ${conversion} <strong>${monedaDestino.toUpperCase()}</strong> por tus ${cantidad} <strong>${monedaOrigen.toUpperCase()}</strong>`
        ),
        boton: "Vender"
    }
}

/* se ejecuta al seleccionar una opcion nueva en el select (nueva moneda)
o al cambiar el input de texto */
function cambioFactor() {
    const monedaOrigen = document.getElementById("origen").value;
    const monedaDestino = document.getElementById("destino").value;
    const operacion = document.getElementById("op_disponibles").value;
    const cantidad = parseFloat(document.getElementById("cantidad").value);
    const conversion = conversiones[monedaDestino][monedaOrigen] * cantidad;
    const mensajes = mensajeOperacion[operacion];
    document.getElementById("operacion").innerHTML = mensajes.resultado(cantidad, conversion, monedaOrigen, monedaDestino);
    document.getElementById("tipo-operacion").innerHTML = mensajes.tipoOperacion;
    document.getElementById("accion").innerHTML = mensajes.boton;
}

function efectuarAccion() {
    /* MODAL */
    var modal = document.getElementById("myModal");
    var span = document.getElementsByClassName("closebutton")[0];

    // Cuando el usuario haga clic en el botón, abre el modal
    modal.style.display = "block";
    // Cuando el usuario haga clic en span (x), cierre el modal
    span.onclick = () => modal.style.display = "none";

    // Cuando el usuario haga clic en cualquier lugar fuera del modal, lo cierra
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}

// toma las llaves presentes en el objeto de conversiones: usd,ars,btc,eth
// y crea opciones (en html) basandose en ellas

function crearHTMLMonedas() {
    const opciones = [];
    Object.keys(conversiones).forEach(
        function (currMoneda) {
            const opcion = document.createElement("option");
            opcion.appendChild(document.createTextNode(currMoneda.toUpperCase()));
            opcion.setAttribute("value", currMoneda);
            opciones.push(opcion);
        }
    )
    return opciones;
}

// toma las opciones creadas en crearMonedas() y las agrega a cada select
// clonandolas con cloneNode(), en el ultimo select toma las opciones reales
//para eque no haga un clone de mas
function cargarMonedasDisponibles() {
    const select = document.getElementsByClassName("opciones-conv");
    const opciones = crearHTMLMonedas();
    [...select].map(function (currSelect, index) {
        const noEsFinal = index < select.length - 1;
        opciones.forEach(function (opc) {
            currSelect.appendChild(
                noEsFinal ? opc.cloneNode(true) : opc
            )
        })
    })
}

window.onload = function () {
    cargarMonedasDisponibles();
    cambioFactor();
}

/*function nuevaMoneda(nombre, valor) {
    // Creamos un diccionario con los valores de las monedas actuales
    toNewCoin = {
        usd: 1 / valor,
        ars: ARS_USD / valor,
        eth: ETH_USD / valor,
        btc: BTC_USD / valor
    }
    toNewCoin[nombre] = toNewCoin; // Agregamos una nueva clave al dicccionario de nueva moneda.
    Object.keys(conversiones).forEach(
        function (moneda) {
            conversiones[moneda][nombre] = toNewCoin[nombre] = parseInt(valor);
        }
    )
    // Agregamos la nueva moneda al diccionario de conversiones
    conversiones[nombre] = toNewCoin;
    console.log(conversiones);
    newCoin = document.getElementById("newCoin");
    //tomo el div con id newCoin para crear el mismo card que tienen las otras monedas

    newCoin.innerHTML += `
        <div  class="card">
        <div class="card-body" id="${nombre}" >
            <div class="card-title">
                <h5>Valor de ${nombre}</h5>
            </div>

            </div>
        </div>
    `
    newCoin.classList.add("col-sm-6", "col-md-3", "col-lg-4", "form-group");
    for (let coinName in conversiones) {
        for (let coinToCoin in conversiones[coinName]) {
            function coinValueFunction(x) {
                return Number.parseFloat(x).toFixed(4)
            }
            var coinValue = conversiones[coinName][coinToCoin];

            document.getElementById(coinName).innerHTML += `
            <p><span class="text-muted fw-bold">${coinToCoin}:</span> ${coinValueFunction(valor)}</p>
            `
        }
    }
    cargarMonedasDisponibles();
}*/

function nuevaMoneda(nombre, valor) {
    // valor debe estar en USD
    // recorrer conversiones crear objeto con conversiones luego del recorrido
    // para convertir entre otras monedas, tomar el valor en usd y dividir entre valor
    borrarMonedasDisponibles();
    let nuevasConversiones = {};
    let convertir;
    conversiones.usd[nombre] = valor;
    for (nombreMoneda in conversiones) {
        convertir = conversiones.usd[nombre] / conversiones.usd[nombreMoneda];
        conversiones[nombreMoneda][nombre] = convertir;
        nuevasConversiones[nombreMoneda] = 1 / convertir;
    }
    nuevasConversiones[nombre] = 1;
    conversiones[nombre] = nuevasConversiones;
    cargarMonedasDisponibles();

}

function borrarMonedasDisponibles() {
    const select = document.getElementsByClassName("opciones-conv");
    [...select].map(
        sel => {
            sel.innerHTML = ""
        }
    )
}

function cargarAPI() {
    document.getElementById("tablaMonedas").innerHTML = ``;
    var listaExchanges = [
        "ArgenBTC",
        "Bitex",
        "Bitmonedero",
        "Bitso",
        "Buda",
        "Buenbit",
        "Copter",
        "CriptoFacil",
        "CryptoMKT",
        "Decrypto",
        "Fiwind",
        "Latamex",
        "LemonCash",
        "Ripio",
        "SatoshiTango",
        "SeSocio",
        "UniversalCoins"];

    for (let moneda of listaExchanges) {
        var api = new XMLHttpRequest();

        api.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                let respuesta = JSON.parse(this.responseText);
                let ask = formateador.format(respuesta["ask"]);
                let totalAsk = formateador.format(respuesta["totalAsk"]);
                let bid = formateador.format(respuesta["bid"]);
                let totalBid = formateador.format(respuesta["totalBid"]);

                document.getElementById("tablaMonedas").innerHTML += `
                    <div id="itemTablaMonedas">
                        <h5><b>${moneda}</b></h5>
                        <!-- Valores adicionales
                        <p>Compra (sin comisiones):<br>${ask}<br>
                        Venta (sin comisiones):<br>${bid}</p>-->
                        <div class="d-flex flex-row justify-content-between">
                            <p><b>Compra:</b><br>${totalAsk}</p>
                            <p><b>Venta:</b><br>${totalBid}</p>
                        </div>
                        <hr>
                    </div>`;
            }
        };
        api.open("GET", "https://criptoya.com/api/" + moneda.toLowerCase(), true);
        api.send();
    }
}

// Formateador de numeros a monedas
var formateador = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
});

/* Crear el objeto que represente a la nueva moneda.
seria un objeto con las conversiones hacia las monedas existentes.
Por ejemplo si quiero agregar euro:
toEur = { ars:, usd:..., btc:..., eth:..., eur:1 }
actualizar los otros elementos existentes para que puedan convertir a euros
habilitar las conversiones para que se pueda hacer conversiones a euros asi
creando la clave eur en el objecto de conversiones que apunte a toEur
conversiones[eur] = toEur
*/
