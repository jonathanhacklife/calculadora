﻿// FUNCIONES PARA MOSTRAR EN EL HTML
function setearResultado(valor) {
  document.getElementById("resultado").innerHTML = valor;
}
function setearInput(valor) {
  console.log(valor)
  document.getElementById("inputCalc").value += valor;
}
// Obtenemos el resultado del String
function obtenerResultado() {
  return (document.getElementById("inputCalc").value);
}
function resetear() {
  document.getElementById("inputCalc").value = ""; // Para resetearlo le mandamos una cadena vacía 
}


// PROGRAMA PARA CALCULAR
function calcularResultado() {
  formula = obtenerResultado(); // Tomar formula del input
  setearResultado(procesarFormula(formula)); // Muestra el resultado en la interfaz 
}

/**
 * La fórmula es la siguiente:
 * 
 * La función procesarFormula2 recibe por parámetro el *valor del input* del HTML.
 * Una vez que tenemos la fórmula, iteramos por cada caracter de la misma.
 * 
 * Cuando en una de esas iteraciones se encuentra con un operador
 * (usamos **isNaN(valor del indice no-numerico)** para obtener el operador)
 * agrega los caracteres desde el índice viejo (el cual sería 0) hasta el índice del operador.
 * 
 *  
 * ¡Ahora ya tenemos el primer operando! ;)
 * 
 * Por consiguiente, guardamos el valor del índice del operador en la variable indiceViejo,
 * para que en la siguiente iteración empiezce desde ahí en adelante y pueda repetir el proceso.
 * 
 * ¡Ahora ya tenemos el segundo operando! :O
 * 
 * Así que, como ya tenemos ambos conjuntos de números, procedemos a switchear el valor del operador
 * para que concluya en la operación correcta.
 * 
 * Ejemplo:
 * "12+34+56" (Sería igual a 102)
 * 
 * Nuestro método lo resolvería de la siguiente manera:
 * 
 * 0 + 12 = 12 (0 es el valor del resultado inicial)
 * 
 * 12 + 34 = 46
 * 
 * 46 + 56 = 102
 */
function procesarFormula(formula) {
  // Algunas variables que vamos a necesitar más adelante.
  var resultado = 0;
  var num2 = 0;
  var indiceViejo = 0;
  var numeros = [];
  var operadores = [];
  var operadoresAceptados = ['+', '-', '/', '*'];

  console.log("Comenzamos descomponiendo la fórmula separando los números de los operadores:");
  // Recorremos el string caracter por caracter.
  for (let i = 0; i <= formula.length; i++) {

    // Si el valor de la posición en la que iteramos no es un número (o sea, si es un operador)
    if (isNaN(formula[i])) {
      /* Al terminar de recorrer la fórmula, no hay más operadores, por eso devuelve undefined
      Así que si ese valor es diferente a undefined: */


      if (formula[i] != undefined) {
        console.log("Encontré el operador", formula[i], "(índice:", i + ")");

        operadores.push(formula[i]); // Método push para agregar un valor a la lista
        console.log("Agrego este operador encontrado:", operadores);
      }
      else {
        console.log("No hay más operadores.")
        console.log(); // Salto de línea para verlo en la consola.
        console.log("Pasamos a operar los números de la lista NUMEROS.")
      }

      // Almacenamos los números desde el índice viejo hasta el índice del operador
      num2 = parseFloat(formula.slice(indiceViejo, i)); // parseFloat convierte un string numérico en número entero. Ej: '12' = 12

      numeros.push(num2);
      console.log("Agrego los numeros desde índice", indiceViejo, "hasta", i, numeros);

      // Ahora el índice viejo va a ser el índice del caracter siguiente
      indiceViejo = i + 1;
    }
  }

  // Por cada operador en la lista de operadores:
  for (let n = 0; n < operadores.length; n++) {

    switch (operadores[n]) {
      case '+': // Si el operador es +
        console.log("Ahora reemplazo el índice 1 (" + numeros[1] + ") de la lista NUMEROS por la suma entre el índice 0 (" + numeros[0] + ") e índice 1 (" + numeros[1] + ")");
        console.log(numeros[0], "+", numeros[1], "=", sumar(numeros[0], numeros[1]));

        numeros[1] = sumar(numeros[0], numeros[1]);
        console.log("NUMEROS =", numeros);

        console.log("Y borro el primer índice");
        numeros.shift(); // Método shift() para quitar el primer elemento de una lista.

        console.log("NUMEROS =", numeros);
        break;
      case '-': // Si el operador es -
        console.log("Ahora reemplazo el índice 1 (" + numeros[1] + ") de la lista NUMEROS por la resta entre el índice 0 (" + numeros[0] + ") e índice 1 (" + numeros[1] + ")");
        console.log(numeros[0], "-", numeros[1], "=", restar(numeros[0], numeros[1]));

        numeros[1] = restar(numeros[0], numeros[1]);
        console.log("NUMEROS =", numeros);

        console.log("Y borro el primer índice");
        numeros.shift(); // Método shift() para quitar el primer elemento de una lista.

        console.log("NUMEROS =", numeros);
        break;
      case '/': // Si el operador es /
        console.log("Ahora reemplazo el índice 1 (" + numeros[1] + ") de la lista NUMEROS por la división entre el índice 0 (" + numeros[0] + ") e índice 1 (" + numeros[1] + ")");
        console.log(numeros[0], "/", numeros[1], "=", dividir(numeros[0], numeros[1]));

        numeros[1] = dividir(numeros[0], numeros[1]);
        console.log("NUMEROS =", numeros);

        console.log("Y borro el primer índice");
        numeros.shift(); // Método shift() para quitar el primer elemento de una lista.

        console.log("NUMEROS =", numeros);
        break;
      case '*': // Si el operador es *
        console.log("Ahora reemplazo el índice 1 (" + numeros[1] + ") de la lista NUMEROS por la multipliación entre el índice 0 (" + numeros[0] + ") e índice 1 (" + numeros[1] + ")");
        console.log(numeros[0], "*", numeros[1], "=", multiplicar(numeros[0], numeros[1]));

        numeros[1] = multiplicar(numeros[0], numeros[1]);
        console.log("NUMEROS =", numeros);

        console.log("Y borro el primer índice");
        numeros.shift(); // Método shift() para quitar el primer elemento de una lista.

        console.log("NUMEROS =", numeros);
        break;
      case '.': // Si el operador es decimal
        console.log("Ahora reemplazo el índice 1 (" + numeros[1] + ") de la lista NUMEROS por la concatenación entre el índice 0 (" + numeros[0] + ") e índice 1 (" + numeros[1] + ")");
        console.log(concatenar(numeros[0], operadores[n], numeros[1]));

        numeros[1] = concatenar(numeros[0], operadores[n], numeros[1]);
        numeros.shift();

        console.log("NUMEROS =", numeros);
        break;
      default:
        numeros.shift(); // Método shift() para quitar el primer elemento de una lista.
        console.log(operadores); // Mostramos la lista de operadores en consola.
        console.log("Fin de la operación");
    }
    resultado = numeros[0] // Guardamos el valor que quedó, en la variable resultado
  }
  console.log("Resultado:", resultado);
  return resultado
}

// Funciones para operar
function sumar(numero1, numero2) {
  return parseFloat(numero1) + parseFloat(numero2);
}
function restar(numero1, numero2) {
  return parseFloat(numero1) - parseFloat(numero2);
}
function multiplicar(numero1, numero2) {
  return parseFloat(numero1) * parseFloat(numero2);
}
function dividir(numero1, numero2) {
  if (parseFloat(numero1) > parseFloat(numero2))
    return parseFloat(numero1) / parseFloat(numero2);
  else {
    return parseFloat(numero2) / parseFloat(numero1);
  }
}

function concatenar(numero1, caracter, numero2) {
  return parseFloat(numero1 + caracter + numero2);
}