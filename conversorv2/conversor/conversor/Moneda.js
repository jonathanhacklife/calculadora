export default class Moneda
{
    // valor en dolares
    tag;
    nombre;
    compra;
    venta;

    constructor({tag,nombre,compra,venta})
    {
        this.tag = tag;
        this.nombre = nombre;
        this.compra = compra;
        this.venta = venta;
    }

    getHtmlOpcion = () => {
        return `<option value="${this.tag}">${this.nombre}</option>`;
    }

    getValorCompra = () => {
        return this.compra;
    }

    getValorVenta = () => {
        return this.venta;
    }

    getTag = () => {
        return this.tag;
    }

    getNombre = () => {
        return this.nombre;
    }
}
