import Conversiones from './Conversiones.js'

const mensajeOperacion = {
    comprar: {
        tipoOperacion: ". Voy a pagar con",
        resultado: (cantidad, conversion, monedaOrigen, monedaDestino) => (
            `Necesitas ${conversion} <strong>${monedaDestino.getNombre()}</strong> para comprar ${cantidad} <strong>${monedaOrigen.getNombre()}</strong>`
        ),
        boton: "Comprar"
    },
    vender: {
        tipoOperacion: "Y obtener",
        resultado: (cantidad, conversion, monedaOrigen, monedaDestino) => (
            `Obtendrás ${conversion} <strong>${monedaDestino.getNombre()}</strong> por tus ${cantidad} <strong>${monedaOrigen.getNombre()}</strong>`
        ),
        boton: "Vender"
    }
}

//maneja html y tiene acceso a conversiones
export default class Conversor
{

    conversiones;

    constructor( monedas )
    {
        this.conversiones = new Conversiones(monedas);
    }

    cargarNuevaMoneda = () => {
        const tag = prompt('Tag de la nueva moneda');
        const nombre = prompt('Nombre de la nueva moneda');
        const compra = prompt('Valor de compra de la nueva moneda en ARS');
        const venta = prompt('Valor de venta de la nueva moneda en ARS');
        return this.nuevaMoneda({tag,nombre,compra,venta});
    }


    efectuarCompraVenta = () => {
        /* MODAL */
        var modal = document.getElementById("myModal");
        var span = document.getElementsByClassName("closebutton")[0];

        // Cuando el usuario haga clic en cualquier lugar fuera del modal, lo cierra
        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }

    cambioFactor = () => {
        const monedaOrigen = this.conversiones.getMoneda(document.getElementById("origen").value);
        const monedaDestino = this.conversiones.getMoneda(document.getElementById("destino").value);
        const operacion = document.getElementById("op_disponibles").value;
        const cantidad = parseFloat(document.getElementById("cantidad").value);
        const conversion = this.conversiones.convertir(monedaOrigen, monedaDestino, operacion) * cantidad;
        const mensajes = mensajeOperacion[operacion];
        document.getElementById("operacion").innerHTML = mensajes.resultado(cantidad, conversion, monedaOrigen, monedaDestino);
        document.getElementById("tipo-operacion").innerHTML = mensajes.tipoOperacion;
        document.getElementById("accionCompraVenta").innerHTML = mensajes.boton;
    }

    crearHTMLMonedas = () => {
        const opciones = [];
        let moneda, opcion;
        for (const tagMoneda in this.conversiones.getConversiones()) {
            moneda = this.conversiones.getMoneda(tagMoneda);
            opcion = moneda.getHtmlOpcion();
            opciones.push(opcion);
        }
        return opciones;
    }


    // Borra las monedas que esten creadas
    // toma las opciones creadas en crearMonedas() y las agrega a cada select
    // clonandolas con cloneNode(), en el ultimo select toma las opciones reales
    // para eque no haga un clone de mas
    cargarMonedasDisponibles = () => {
        const select = document.getElementsByClassName("opciones-conv");
        const opciones = this.crearHTMLMonedas();
            [...select].map(function (currSelect, index) {
                opciones.forEach(function (opc) {
                    currSelect.insertAdjacentHTML('beforeend', opc)
                })
            })
    }

    borrarMonedasDisponibles = () => {
        const select = document.getElementsByClassName("opciones-conv");
            [...select].map(
                sel => {
                    sel.innerHTML = ""
                }
            )
    }

    nuevaMoneda = (moneda) => {
        this.borrarMonedasDisponibles();
        this.conversiones.agregarMoneda(moneda)
        this.cargarMonedasDisponibles();
    }

    resetMonedas = (monedas) => {
        this.borrarMonedasDisponibles();
        this.conversiones.resetMonedas(monedas);
        this.cargarMonedasDisponibles();
    }

    crearCartasConversionesMonedas = () => {
        const {monedas} = this.conversiones
        let moneda;
        for (let coinName in monedas) {
            moneda = monedas[coinName];
            document.getElementById("available-coins").innerHTML += `
                <div style="border-bottom:1px solid rgba(0,0,0,.1)" class="col-md-12 py-2 px-0">
                    <div id="${moneda.getTag()}" >
                        <div class="card-title">
                            <h6><strong>${moneda.getNombre()}</strong></h6>
                        </div>
                        <div>
                            <strong>compra: </strong>${moneda.getValorCompra()}<strong> ARS</strong><br>
                            <strong>venta: </strong>${moneda.getValorVenta()}<strong> ARS</strong>
                        </div>
                    </div>
                </div>`
        }
    }

    invertirMonedas = () => {
        let aux;
        const origen = document.getElementById("origen");
        const destino = document.getElementById("destino");
        aux = origen.value;
        origen.value = destino.value;
        destino.value = aux;
        this.cambioFactor();
    }

}
