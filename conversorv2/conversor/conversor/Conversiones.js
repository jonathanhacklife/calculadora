import Moneda from './Moneda.js';

// tiene logica de conversion y abstrae sobre moneda
export default class Conversiones {

    monedas;

    constructor( monedas )
    {
        this.inicializarConversiones(monedas)
    }

    resetMonedas = (monedas) => {
        this.inicializarConversiones(monedas);
    }

    // inicializa con las conversiones por defecto
    inicializarConversiones = (monedas) => {
        this.monedas = {};
        for (let monedaObj of monedas)
        {
            this.monedas[monedaObj.tag] = new Moneda(monedaObj);
        }
    }

    agregarMoneda = ({ tag, compra, venta, nombre }) => {
        //probable crash
        const moneda = new Moneda({ tag, compra, venta, nombre });
        this.monedas[moneda.getTag()] = moneda;
        return this;
    }

    getMoneda = (tag) => {
        return this.monedas[tag];
    }

    getConversiones = () => {
        return this.monedas;
    }

    // operacion puede ser compra/venta
    convertir = (monedaOrigen, monedaDestino, operacion)  => {
        return (operacion ==='vender')
            ? monedaOrigen.getValorCompra()/monedaDestino.getValorVenta()
            : monedaOrigen.getValorVenta()/monedaDestino.getValorCompra();
    }

}
