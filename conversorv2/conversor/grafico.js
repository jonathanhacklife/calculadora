
var grafico;

export function mostrarEnGrafico(valores) {
    var listaMonedas = [];
    var precioCompra = [];
    var precioVenta = [];

    for (let moneda of valores)
    {
        precioCompra.push(moneda.compra)
        precioVenta.push(moneda.venta)
        listaMonedas.push(moneda.nombre)
    }

    var graficoHTML = document.getElementById('graficoCrypto');
    /* Creamos el gráfico una vez */
    if (grafico == null) {
        grafico = new Chart(graficoHTML, {
            type: 'line',
            data: {
                labels: listaMonedas, /* MONEDAS */
                datasets: [{
                    label: 'Compra',
                    data: precioCompra, /* VALOR */
                    fill: false,
                    borderColor: 'rgb(81, 53, 165)',
                    tension: 0.1,
                    pointRadius: 7,
                    pointHoverRadius: 15
                }, {
                label: 'Venta',
                    data: precioVenta, /* VALOR */
                    fill: true,
                    borderColor: 'rgb(246, 224, 53)',
                    tension: 0.1,
                    pointRadius: 7,
                    pointHoverRadius: 15
                }]
            },
            options: {
                tooltips: {
                    mode: 'index',
                    intersect: true,
                },
                scales: {
                    y: {
                        beginAtZero: true,
                    }
                }
            }
        });
    } else {
        /* Actualizamos los datos del gráfico */
        grafico.config.data.datasets[0].data = precioCompra;
        grafico.config.data.datasets[1].data = precioVenta;

    }
}
