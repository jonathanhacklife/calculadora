const listaExchanges = [
    {tag:"argenbtc",nombre:"Argen BTC"},
    {tag:"bitex",nombre:"Bitex"},
    {tag:"bitmonedero",nombre:"Bitmonedero"},
    {tag:"bitso",nombre:"Bitso"},
    {tag:"buda",nombre:"Buda"},
    {tag:"buenbit",nombre:"Buen Bit"},
    {tag:"copter",nombre:"Copter"},
    {tag:"criptofacil",nombre:"CriptoFacil"},
    {tag:"cryptomkt",nombre:"Crypto MKT"},
    {tag:"decrypto",nombre:"Decrypto"},
    {tag:"fiwind",nombre:"Fiwind"},
    {tag:"latamex",nombre:"Latamex"},
    {tag:"lemoncash",nombre:"Lemon Cash"},
    {tag:"ripio",nombre:"Ripio"},
    {tag:"satoshitango",nombre:"Satoshi Tango"},
    {tag:"sesocio",nombre:"Se Socio"},
    {tag:"universalcoins",nombre:"Universal Coins"}
];

function buscarMonedaEnApi( moneda )
{
    return new Promise(
        (resolve,reject) => {
            var api = new XMLHttpRequest();
            api.open("GET", `https://criptoya.com/api/${moneda.tag}`, true);
            api.send();
            api.onerror = function() {
                reject({error:'Error del servidor'})
            }
            api.onload = function () {
                if (this.status === 200)
                {
                    const {ask,bid} = JSON.parse(this.responseText)
                    resolve({compra:bid,venta:ask,...moneda})
                }
                reject({error:'Consulta invalida'})
            }
        }
    )
}

export default async function cargarMonedasDesdeApi()
{
    let mon;
    const res = [];
    for (let moneda of listaExchanges)
    {
        try {
            mon = await buscarMonedaEnApi(moneda);
            res.push(mon);
        } catch (e) {
            console.error("error:",e)
        }
    }
    return res;
}
