import Conversor from './conversor/Conversor.js'

import {mostrarEnGrafico} from './grafico.js'

import cargarMonedasDesdeApi from './api/handlersApi.js'

export function cargarHandlers(conversor) {
    conversor.crearCartasConversionesMonedas();
    conversor.cargarMonedasDisponibles();
    conversor.cambioFactor();
    document.getElementById('nuevaMoneda').onclick = conversor.cargarNuevaMoneda;
    document.getElementById('accionCompraVenta').onclick = conversor.efectuarCompraVenta;
    document.getElementById('invertirMoneda').onclick = conversor.invertirMonedas;
    [...document.getElementsByClassName('input-select')].forEach(
        function (select) {
            select.onchange = conversor.cambioFactor;
        }
    )
    document.getElementById("recargarMonedas").onclick = async function(){
        let monedas;
        if (!this.disabled)
        {
            this.disabled = true;
            monedas = await cargarMonedasDesdeApi();
            conversor.resetMonedas(monedas);
            mostrarEnGrafico(monedas);
            this.disabled = false;
        }
    }
}

window.onload = async function () {
    let monedas = await cargarMonedasDesdeApi();
    let conversor = new Conversor(monedas);
    mostrarEnGrafico(monedas);
    cargarHandlers(conversor);
}
