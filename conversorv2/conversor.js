// conversion ars -> usd
const ARS_USD = 1 / 140;      // 1 ars = 1/140 usd

// conversion btc -> usd
const BTC_USD = 56801.00;   // 1 btc = 56801.00 usd

// conversion eth -> usd
const ETH_USD = 1790.76;    // 1 eth = 1790.76 usd

// conversiones desde usd,ars,eth,btc a btc
const toBtc = {
    usd: 1 / BTC_USD,
    ars: ARS_USD / BTC_USD,
    eth: ETH_USD / BTC_USD,
    btc: 1
}

// conversiones desde usd,ars,eth,btc a eth
const toEth = {
    usd: 1 / ETH_USD,
    ars: ARS_USD / ETH_USD,
    eth: 1,
    btc: BTC_USD / ETH_USD
}

// conversiones desde usd,ars,eth,btc a ars
const toArs = {
    usd: 1 / ARS_USD,
    ars: 1,
    eth: ETH_USD / ARS_USD,
    btc: BTC_USD / ARS_USD
}

// conversiones desde usd,ars,eth,btc a usd
const toUsd = {
    usd: 1,
    ars: ARS_USD,
    eth: ETH_USD,
    btc: BTC_USD
}

// mapea las conversiones en general
const conversiones = {
    usd: toUsd,
    ars: toArs,
    btc: toBtc,
    eth: toEth
};

function strong( texto )
{
    return `<strong>${texto}</strong>`
}

const mensajeOperacion = {
    comprar:{
        tipoOperacion:". Voy a pagar con",
        resultado:( cantidad,conversion,monedaOrigen,monedaDestino ) => (
            `Necesitas ${conversion}${strong(monedaDestino.toUpperCase())} para comprar ${cantidad}${strong(monedaOrigen.toUpperCase())}`
        ),
        boton:"Comprar"
    },
    vender: {
        tipoOperacion:"Y obtener",
        resultado:( cantidad,conversion,monedaOrigen,monedaDestino ) => (
            `Obtendrás ${conversion}${strong(monedaDestino.toUpperCase())} por tus ${cantidad}${strong(monedaOrigen.toUpperCase())}`
        ),
        boton:"Vender"
    }
}

// se ejecuta al seleccionar una opcion nueva en el select (nueva moneda)
// o al cambiar el input de texto
function cambioFactor()
{
    const monedaOrigen = document.getElementById("origen").value;
    const monedaDestino = document.getElementById("destino").value;
    const operacion = document.getElementById("op_disponibles").value;
    const cantidad = parseFloat(document.getElementById("cantidad").value);
    const conversion = conversiones[monedaDestino][monedaOrigen] * cantidad;
    const mensajes = mensajeOperacion[operacion];
    document.getElementById("operacion").innerHTML = mensajes.resultado(cantidad,conversion,monedaOrigen,monedaDestino);
    document.getElementById("tipo-operacion").innerHTML = mensajes.tipoOperacion;
    document.getElementById("accion").innerHTML = mensajes.boton;
}

window.onload = cambioFactor();
