import Conversor from './conversor/Conversor.js'

import {
    crearCartasConversionesMonedas,
    agregarValoresACartasDeConversion
} from './conversor/handlersCartasConversiones.js'

const conversor = new Conversor();

function efectuarCompraVenta() {
    /* MODAL */
    var modal = document.getElementById("myModal");
    var span = document.getElementsByClassName("closebutton")[0];

    // Cuando el usuario haga clic en cualquier lugar fuera del modal, lo cierra
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}

function cargarNuevaMonedaHandler() {
    const tag = prompt('Tag de la nueva moneda');
    const nombre = prompt('Nombre de la nueva moneda');
    const valorUsd = prompt('Valor de la nueva moneda en USD');
    return conversor.nuevaMoneda({tag,nombre,valorUsd});
}

function cargarCambioFactorHandler(handler) {
    [...document.getElementsByClassName('input-select')].forEach(
        function (select) {
            select.onchange = handler;
        }
    )
}

function cargarHandlers() {
    document.getElementById('nuevaMoneda').onclick = cargarNuevaMonedaHandler;
    document.getElementById('accionCompraVenta').onclick = efectuarCompraVenta;
    cargarCambioFactorHandler(conversor.cambioFactor);
}

/* function mostrarEnPantalla(valores) {
    document.getElementById("tablaMonedas").innerHTML = ``;

    for (let moneda of Object.keys(valores)) {
        document.getElementById("tablaMonedas").innerHTML += `
        <div id="itemTablaMonedas">
            <h5><b>${moneda}</b></h5>
            <div class="d-flex flex-row justify-content-between">
                <p><b>Compra:</b><br>${formateador.format(valores[moneda]["compra"])}</p>
                <p><b>Venta:</b><br>${formateador.format(valores[moneda]["venta"])}</p>
            </div>
            <hr>
        </div>`;
    }
}

// Formateador de numeros a monedas
var formateador = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD'
});
*/

/* Crear el objeto que represente a la nueva moneda.
seria un objeto con las conversiones hacia las monedas existentes.
Por ejemplo si quiero agregar euro:
toEur = { ars:, usd:..., btc:..., eth:..., eur:1 }
actualizar los otros elementos existentes para que puedan convertir a euros
habilitar las conversiones para que se pueda hacer conversiones a euros asi
creando la clave eur en el objecto de conversiones que apunte a toEur
conversiones[eur] = toEur
*/
window.onload = function () {
    cargarHandlers();
    crearCartasConversionesMonedas();
    agregarValoresACartasDeConversion();
    conversor.cargarMonedasDisponibles();
    conversor.cambioFactor();
}
