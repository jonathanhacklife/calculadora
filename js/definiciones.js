//Arreglos que toman todos los botones de números y operadores
export const btnNumber = document.getElementsByName('btnNumber');
export const btnOperator = document.getElementsByName('btnOperator');
//Arreglos que toman botones de = y C, agarrando sólo elprimero (índice 0)
export const btnResult = document.getElementsByName('btnResult')[0];
export const btnReset = document.getElementsByName('btnReset')[0];

/* Botones de operación */
export const btnSuma = document.getElementById('+');
export const btnResta = document.getElementById('-');
export const btnMultiplicacion = document.getElementById('*');
export const btnDivision = document.getElementById('/');
export const btnPotencia = document.getElementById('pow');
export const btnRaiz = document.getElementById('expo');
export const btnLog2 = document.getElementById('log2');
export const btnLog10 = document.getElementById('log10');

//Capturo el resultado. Lo tomo con  var porque el resultado cambiará según la cuenta.
export var input = document.getElementById('inputCalc');

//guardo operaciones en export variables
export var operacion = undefined;


export default {
    btnNumber, btnOperator, btnResult, btnReset, btnSuma, btnResta, btnMultiplicacion, btnDivision, btnPotencia, btnRaiz, btnLog2, btnLog10, input, operacion
}