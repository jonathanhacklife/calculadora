import * as btn from './definiciones.js'
/* POO */
class Calculadora {
    static op1;
    static op2;
    static res;

    opActual;
    opAnterior;
    operacion;
    op;

    constructor(op1, op2) {
        this.op1 = parseFloat(op1);
        this.op2 = parseFloat(op2);
        this.res = null;
        this.opActual = '';
        this.opAnterior = '';
        this.operacion = '';
        this.op = '';
    }

    setResultado(numero) {
        this.res = numero;
        return this.res;
    }

    mostrarResultado() {
        console.log(this.res);
        return this.res;
    }

    calcular() {
        var calculo;
        if (isNaN(parseFloat(this.opAnterior)) || isNaN(parseFloat(this.opActual))) return;
        switch (this.operacion) { //paso por las cuatro operaciones y realizo los cálculos
            case '+':
                calculo = new Suma(this.opAnterior, this.opActual);
                break;
            case '-':
                calculo = new Resta(this.opAnterior, this.opActual);
                break;
            case '*':
                calculo = new Multiplicacion(this.opAnterior, this.opActual);
                break;
            case '/':
                calculo = new Division(this.opAnterior, this.opActual);
                break;
            case 'x^':
                calculo = new Potencia(this.opAnterior, this.opActual);
                break;
            case '√':
                calculo = new Raiz(this.opAnterior, this.opActual);
                break;
            case 'log2':
                calculo = new Log2(this.opAnterior);
                break;
            case 'log10':
                calculo = new Log10(this.opAnterior);
                break;
            default:
                return;
        }
        this.opActual = calculo.mostrarResultado(); //cambio el contenido del input por el resultado del cálculo
        chequearOperador(this.opActual); // Chequea al obtener resultado
        this.operacion = undefined;
        this.opAnterior = '';
        this.actualizarDisplay();
    }

    resetear() { //Si aprieto C reseteo el input, 
        this.opActual = '';
        this.opAnterior = '';
        this.operacion = undefined;
        this.actualizarDisplay();
    }

    actualizarDisplay() {
        btn.input.value = this.opActual; //muestra el valor del resultado.
    }

    selectOperator(op) {
        if (this.opActual === '') return; //si en el input no hay nada resetea
        if (this.opAnterior !== '') { //si la operación anterior no está vacía
            this.calcular()
        }
        this.operacion = op.toString();
        this.opAnterior = this.opActual;
        this.opActual = '';
    }

    agregarNumero(num) {
        this.opActual = this.opActual.toString() + num.toString(); //capturo el valor en formato texto porque el input de resultado es type="text". La suma es para ir concatenando los números que presione
        chequearOperador(this.opActual); // Chequea al presionarse un número
        this.actualizarDisplay();
    }
    // resetear()
}

/* HERENCIA */
class Suma extends Calculadora {
    // Creamos un constructor
    constructor(op1, op2) {
        // Llamamos a las propiedades del padre (Calculadora)
        super(op1, op2);
        // Le pasamos el resultado de la operación
        this.setResultado(this.op1 + this.op2);
    }
}

class Resta extends Calculadora {
    constructor(op1, op2) {
        super(op1, op2);
        this.setResultado(this.op1 - this.op2);
    }
}

class Multiplicacion extends Calculadora {
    constructor(op1, op2) {
        super(op1, op2);
        this.setResultado(this.op1 * this.op2);
    }
}

class Division extends Calculadora {
    constructor(op1, op2) {
        super(op1, op2);
        if (this.op2 != 0)
            this.setResultado(this.op1 / this.op2);
    }
}

class Potencia extends Calculadora {
    constructor(op1, op2) {
        super(op1, op2);
        this.setResultado(Math.pow(this.op1, this.op2));
    }
}

class Raiz extends Calculadora {
    constructor(op1, op2) {
        super(op1, op2);
        this.setResultado(Math.sqrt(this.op1, this.op2));
    }
}

class Log2 extends Calculadora {
    constructor(op1, op2) {
        super(op1, op2);
        this.setResultado(Math.log(this.op1, 2));
    }
}

class Log10 extends Calculadora {
    constructor(op1, op2) {
        super(op1, op2);
        this.setResultado(Math.log(this.op1, 10));
    }
}

let calculadora = new Calculadora();
// console.log(calculadora.setResultado)

btn.btnNumber.forEach(function (boton) { //por cada elemento de btnNumber tomo el botón y le genero un evento
    boton.addEventListener('click', function () { //cuando clickeo el botón genera una función
        calculadora.agregarNumero(boton.innerText); //envía al input el texto que contiene el botón presionado
    })
})

btn.btnOperator.forEach(function (boton) {
    boton.addEventListener('click', function () {
        calculadora.selectOperator(boton.innerText);
    })
})

btn.btnResult.addEventListener('click', function () {
    calculadora.calcular();
})

btn.btnReset.addEventListener('click', function () {
    calculadora.resetear();
})

function chequearOperador(num) {
    // para consigna 23/03

    switch (parseFloat(num)) {
        case 777:
            cambiarBackground("money");
            break;
        case 666:
            // mostrar imagen
            cambiarBackground("hell");
            break;
        case 911:
            // mostrar imagen
            cambiarBackground("police");
            break;
        case 0:
            cambiarBackground("eyes");
            break;
        default:
            resetearBackground()
            break;
    }
}

function cambiarBackground(myclass) {
    var element = document.getElementById("body");
    element.className = myclass;
}

function resetearBackground() {
    var element = document.getElementById("body");
    element.className = "";
}
