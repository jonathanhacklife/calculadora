const ARS_USD = 1 / 140;      // 1 ars = 1/140 usd

// conversion btc -> usd
const BTC_USD = 56801.00;   // 1 btc = 56801.00 usd

// conversion eth -> usd
const ETH_USD = 1790.76;    // 1 eth = 1790.76 usd

// conversiones desde usd,ars,eth,btc a btc
const toBtc = {
    usd: 1 / BTC_USD,
    ars: ARS_USD / BTC_USD,
    eth: ETH_USD / BTC_USD,
    btc: 1
}

// conversiones desde usd,ars,eth,btc a eth
const toEth = {
    usd: 1 / ETH_USD,
    ars: ARS_USD / ETH_USD,
    eth: 1,
    btc: BTC_USD / ETH_USD
}

// conversiones desde usd,ars,eth,btc a ars
const toArs = {
    usd: 1 / ARS_USD,
    ars: 1,
    eth: ETH_USD / ARS_USD,
    btc: BTC_USD / ARS_USD
}

// conversiones desde usd,ars,eth,btc a usd
const toUsd = {
    usd: 1,
    ars: ARS_USD,
    eth: ETH_USD,
    btc: BTC_USD
}

// mapea las conversiones en general
const conversiones = {
    usd: toUsd,
    ars: toArs,
    btc: toBtc,
    eth: toEth
};


export function crearCartasConversionesMonedas() {
  for (let coinName in conversiones) {
    document.getElementById("coin").innerHTML += `
            <div class="col-sm-6 col-md-3 col-lg-4 form-group">
                <div  class="card">
                <div class="card-body" id="${coinName}" >
                    <div class="card-title">
                        <h5>Valor de ${coinName}</h5>
                    </div>
                    </div>
                </div>
            </div>`
  }
}

export function agregarValoresACartasDeConversion() {
  for (let coinName in conversiones) {
    for (let coinToCoin in conversiones[coinName]) {
      function coinValueFunction(x) {
        return Number.parseFloat(x).toFixed(4)
      }
      var coinValue = conversiones[coinName][coinToCoin];

      document.getElementById(coinName).innerHTML += `
            <p><span class="text-muted fw-bold">${coinToCoin}:</span> ${coinValueFunction(coinValue)}</p>
            `
    }
  }
}