import Conversiones from './Conversiones.js'

const mensajeOperacion = {
  comprar: {
    tipoOperacion: ". Voy a pagar con",
    resultado: (cantidad, conversion, monedaOrigen, monedaDestino) => (
      `Necesitas ${conversion} <strong>${monedaDestino.getNombre()}</strong> para comprar ${cantidad} <strong>${monedaOrigen.getNombre()}</strong>`
    ),
    boton: "Comprar"
  },
  vender: {
    tipoOperacion: "Y obtener",
    resultado: (cantidad, conversion, monedaOrigen, monedaDestino) => (
      `Obtendrás ${conversion} <strong>${monedaDestino.getNombre()}</strong> por tus ${cantidad} <strong>${monedaOrigen.getNombre()}</strong>`
    ),
    boton: "Vender"
  }
}

//maneja html y tiene acceso a conversiones
export default class Conversor {

    conversiones;

    constructor() {
        this.conversiones = new Conversiones();
    }

    cambioFactor = () => {
        const monedaOrigen = this.conversiones.getMoneda(document.getElementById("origen").value);
        const monedaDestino = this.conversiones.getMoneda(document.getElementById("destino").value);
        const operacion = document.getElementById("op_disponibles").value;
        const cantidad = parseFloat(document.getElementById("cantidad").value);
        const conversion = this.conversiones.convertir(monedaOrigen, monedaDestino) * cantidad;
        const mensajes = mensajeOperacion[operacion];
        document.getElementById("operacion").innerHTML = mensajes.resultado(cantidad, conversion, monedaOrigen, monedaDestino);
        document.getElementById("tipo-operacion").innerHTML = mensajes.tipoOperacion;
        document.getElementById("accionCompraVenta").innerHTML = mensajes.boton;
    }

    crearHTMLMonedas = () => {
    const opciones = [];
    let moneda, opcion;
        for (const tagMoneda in this.conversiones.getConversiones()) {
            moneda = this.conversiones.getMoneda(tagMoneda);
            opcion = moneda.getHtmlOpcion();
            opciones.push(opcion);
        }
        return opciones;
    }


    // Borra las monedas que esten creadas
    // toma las opciones creadas en crearMonedas() y las agrega a cada select
    // clonandolas con cloneNode(), en el ultimo select toma las opciones reales
    // para eque no haga un clone de mas
    cargarMonedasDisponibles = () => {
        const select = document.getElementsByClassName("opciones-conv");
        const opciones = this.crearHTMLMonedas();
            [...select].map(function (currSelect, index) {
                opciones.forEach(function (opc) {
                    currSelect.insertAdjacentHTML('beforeend', opc)
                })
            })
    }

    borrarMonedasDisponibles = () => {
    const select = document.getElementsByClassName("opciones-conv");
        [...select].map(
            sel => {
                sel.innerHTML = ""
            }
        )
    }

    nuevaMoneda = ({ tag, nombre, valorUsd }) => {
        this.borrarMonedasDisponibles();
        this.conversiones.agregarMoneda({ tag, nombre, valorUsd })
        this.cargarMonedasDisponibles();
    }


}
