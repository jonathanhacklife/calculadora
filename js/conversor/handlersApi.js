export function cargarAPI(callback) {
  var contador = 0;

  var listaExchanges = [
    "ArgenBTC",
    "Bitex",
    "Bitmonedero",
    "Bitso",
    "Buda",
    "Buenbit",
    "Copter",
    "CriptoFacil",
    "CryptoMKT",
    "Decrypto",
    "Fiwind",
    "Latamex",
    "LemonCash",
    "Ripio",
    "SatoshiTango",
    "SeSocio",
    "UniversalCoins"];

  var valorLocal = {};
  for (let moneda of listaExchanges) {
    var api = new XMLHttpRequest();

    api.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        let respuesta = JSON.parse(this.responseText);
        let ask = respuesta["ask"];
        let bid = respuesta["bid"];

        valorLocal[moneda] = { compra: ask, venta: bid }
        contador++;
        if (contador == listaExchanges.length) {

          /* Ordenamos la lista de monedas */
          const ordenado = Object.keys(valorLocal).sort().reduce(
            (obj, key) => {
              obj[key] = valorLocal[key];
              return obj;
            }, {}
          );
          callback(ordenado);
        }
      }
    };
    api.open("GET", "https://criptoya.com/api/" + moneda.toLowerCase(), true);
    api.send();
  }
}
