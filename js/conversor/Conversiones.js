import Moneda from './Moneda.js';

// conversion ars -> usd
const ARS_USD = 1 / 140;      // 1 ars = 1/140 usd

// conversion btc -> usd
const BTC_USD = 56801.00;   // 1 btc = 56801.00 usd

// conversion eth -> usd
const ETH_USD = 1790.76;    // 1 eth = 1790.76 usd

// tiene logica de conversion y abstrae sobre moneda
export default class Conversiones {

    monedas;

    constructor() {
        this.inicializarConversiones();
    }

    // inicializa con las conversiones por defecto
    inicializarConversiones() {
        this.monedas = {
            btc: new Moneda({ tag: "btc", nombre: "Bitcoin", valorUsd: BTC_USD }),
            ars: new Moneda({ tag: "ars", nombre: "Pesos", valorUsd: ARS_USD }),
            eth: new Moneda({ tag: "eth", nombre: "Ethereum", valorUsd: ETH_USD }),
            usd: new Moneda({ tag: "usd", nombre: "Dolares", valorUsd: 1 })
        }
    }

    agregarMoneda({ tag, valorUsd, nombre }) {
        //probable crash
        const moneda = new Moneda({ tag, valorUsd, nombre });
        this.monedas[moneda.getTag()] = moneda;
        return this;
    }

    getMoneda(tag) {
        return this.monedas[tag];
    }

    getConversiones() {
        return this.monedas;
    }

    convertir(monedaOrigen, monedaDestino) {
        return monedaOrigen.getValor() / monedaDestino.getValor();
    }

}
