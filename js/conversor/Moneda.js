export default class Moneda 
{
  // valor en dolares
  tag;
  nombre;
  valor;

  constructor({tag,nombre,valorUsd}) 
  {
    this.tag = tag;
    this.nombre = nombre;
    this.valor = valorUsd;
  }

  getHtmlOpcion = () => {
    return `<option value="${this.tag}">${this.nombre}</option>`;
  }

  getValor = () => {
    return this.valor;
  }

  getTag = () => {
    return this.tag;
  }

  getNombre = () => {
    return this.nombre;
  }
}