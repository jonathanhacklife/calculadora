﻿/*
TODO:
  -Corroborar que la entrada sea un número
  -Corroborar que no se escriban caracteres alfabeticos
  -Permitir que la operacion funcione tanto escribiendo en el input como con los botones
  PARA HACER: dividir en operaciones primarias: /, * y secundarias: + y *
*/
const OPERACIONES_OBJ = {
  '+':(n1,n2) => n1+n2,
  '-':(n1,n2) => n1-n2,
  '/':(n1,n2) => n1/n2,
  '*':(n1,n2) => n1*n2
};

const NUMEROS = [
  '0', '1', '2', '3', '4', '5', '6','7', '8', '9'
];

const CONTROL = [
    'Backspace','Enter','.','ArrowLeft','ArrowRight','Delete'
];

const OPERACIONES = Object.keys(OPERACIONES_OBJ);

// FUNCIONES PARA MOSTRAR EN EL HTML
function setearResultado(valor) {
  document.getElementById("resultado").innerHTML = valor;
}
function setearInput(valor) {
  document.getElementById("inputCalc").value += valor;
}
// Obtenemos el resultado del String
function obtenerResultado() {
  return (document.getElementById("inputCalc").value);
}
function resetear() {
  setearResultado(""); // Para resetearlo le mandamos una cadena vacía
}

function obtenerActual( formula,posicion,caracter=formula[posicion] ){
    let actual = "",esNegativo;
    while(OPERACIONES.indexOf(caracter)<0 && posicion>=0){ // concatena el numero
        actual+=caracter;
        posicion--;
        caracter = formula[posicion];
    }
    return caracter==='-'
        ? [ parseFloat('-'+actual),posicion,'+' ]
        : [ parseFloat(actual),posicion,caracter ];
}

function procesarFormula( formula,op,posicion=formula.length-1 ) {
    let actual,operacion;
    [actual,posicion,operacion] = obtenerActual( formula,posicion );
    if (posicion>0) //aplicar recursion a la operacion en curso
        return op[operacion]( actual,procesarFormula(formula,op,posicion-1) )
    return actual;//caso base, si me excedo retornar el numero concatenado
}
/*
    11 + 23 - 1 / 4
    digamos q  operacion se refiere a sumar/restar/dividir
    suma( 11, resta( 23, division(1, 4) ) )
*/

function aplicarOperaciones( formula ){
    const primarias = procesarFormula(formula,OPERACIONES_OBJ_PRIMARIAS);
    const
}

function calcularResultado() {
    const formula = document.getElementById("inputCalc").value; // Tomar formula del input
    resultado = procesarFormula(formula); // Muestra el resultado en la interfazs
    console.log(eval(formula))
    console.log(resultado)
}

function procesarTecla(){
    const validos_chequeo = NUMEROS.concat(OPERACIONES).concat(CONTROL);
    // const formula = document.getElementById("inputCalc").value;
    if( validos_chequeo.indexOf(event.key)<0 )
        event.preventDefault();
    if( event.keyCode===13 )// && !OPERACIONES[formula[-1]] )
        calcularResultado();

}

/*! Otra forma de resolverlo (Próximamente)
function procesarFormula2(formula) {
  for (char of formula) {
    if (isNaN(char)) { }
  }

  return resultado;
}
*/
