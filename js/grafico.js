﻿import { cargarAPI } from './conversor/handlersApi.js'

/* Cargamos el gráfico al inicio */
cargarAPI(function (dato) { mostrarEnGrafico(dato) });

/* Refrescamos el gráfico cada 5 segundos */
setInterval(function () { cargarAPI(function (dato) { mostrarEnGrafico(dato) }) }, 60000)

var grafico;

function mostrarEnGrafico(valores) {
  var listaMonedas = Object.keys(valores);
  var precioCompra = [];
  var precioVenta = [];

  for (let moneda of listaMonedas) {
    precioCompra.push(valores[moneda]["compra"])
    precioVenta.push(valores[moneda]["venta"])
  }

  var graficoHTML = document.getElementById('graficoCrypto');
  /* Creamos el gráfico una vez */
  if (grafico == null) {
    grafico = new Chart(graficoHTML, {
      type: 'line',
      data: {
        labels: listaMonedas, /* MONEDAS */
        datasets: [{
          label: 'Compra',
          data: precioCompra, /* VALOR */
          fill: false,
          borderColor: 'rgb(81, 53, 165)',
          tension: 0.1,
          pointRadius: 7,
          pointHoverRadius: 15
        }, {
          label: 'Venta',
          data: precioVenta, /* VALOR */
          fill: true,
          borderColor: 'rgb(246, 224, 53)',
          tension: 0.1,
          pointRadius: 7,
          pointHoverRadius: 15
        }]
      },
      options: {
        tooltips: {
          mode: 'index',
          intersect: true,
        },

        scales: {
          y: {
            beginAtZero: true,
          }
        }
      }
    });
  } else {
    /* Actualizamos los datos del gráfico */
    grafico.config.data.datasets[0].data = precioCompra;
    grafico.config.data.datasets[1].data = precioVenta;

  }
}