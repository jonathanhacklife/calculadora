//TODO: poder escribir el cálculo en el input sin usar los botones

//Arreglos que toman todos los botones de números y operadores
const btnNumber = document.getElementsByName('btnNumber');
const btnOperator = document.getElementsByName('btnOperator');
//Arreglos que toman botones de = y C, agarrando sólo elprimero (índice 0)
const btnResult = document.getElementsByName('btnResult')[0];
const btnReset = document.getElementsByName('btnReset')[0];

//Capturo el resultado. Lo tomo con var porque el resultado cambiará según la cuenta.
var result = document.getElementById('inputCalc');

//guardo operaciones en variables
var opActual = '';
var opAnterior = '';
var operacion = undefined;

btnNumber.forEach(function (boton) { //por cada elemento de btnNumber tomo el botón y le genero un evento
    boton.addEventListener('click', function () { //cuando clickeo el botón genera una función
        agregarNumero(boton.innerText); //envía al input el texto que contiene el botón presionado

    })
})

btnOperator.forEach(function (boton) {
    boton.addEventListener('click', function () {
        selectOperator(boton.innerText);
    })
})

btnResult.addEventListener('click', function () {
    calcular();
    actualizarDisplay();
})

btnReset.addEventListener('click', function () {
    resetear();
    actualizarDisplay();
})

function agregarNumero(num) {
    opActual = opActual.toString() + num.toString(); //capturo el valor en formato texto porque el input de resultado es type="text". La suma es para ir concatenando los números que presione
    actualizarDisplay();
}

function calcular() {
    var calculo;
    const anterior = parseFloat(opAnterior); //Marco que puedan ser números no enteros
    const actual = parseFloat(opActual);
    if (isNaN(anterior) || isNaN(actual)) return;
    switch (operacion) { //paso por las cuatro operaciones y realizo los cálculos
        case '+':
            calculo = anterior + actual;
            break;
        case '-':
            calculo = anterior - actual;
            break;
        case '*':
            calculo = anterior * actual;
            break;
        case '/':
            calculo = anterior / actual;
            break;
        default:
            return;
    }
    opActual = calculo; //cambio el contenido del input por el resultado del cálculo
    operacion = undefined;
    opAnterior = '';
}

function resetear() { //Si aprieto C reseteo el input, 
    opActual = '';
    opAnterior = '';
    operacion = undefined;
}


function actualizarDisplay() {
    result.value = opActual; //muestra el valor del resultado.
}

function selectOperator(op) {
    if (opActual === '') return; //si en el input no hay nada resetea
    if (opAnterior !== '') { //si la operación anterior no está vacía
        calcular()
    }
    operacion = op.toString();
    opAnterior = opActual;
    opActual = '';

}

resetear();