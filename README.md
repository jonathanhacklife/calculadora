# Javascript

Este taller está siendo elaborado frente a la necesidad de capacitar a más personas en CyS en esta tecnología. El taller se va a dividir en 10 dias intensivos con parte teórica y práctica, centrándose en los conceptos principales de JS. Los talleres se desarrollarán de forma virtual.

## Requerimientos

- Sistema operativo: Windows, Ubuntu (Preferentemente), MacOs.
- Visual Studio Code
- Extensión Live Server (https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)
- Conceptos básicos de programación

¿Con habilidades va a contar el cursante al finalizar el taller?

## Temario por día

A continuación, se detallará los contenidos dictados día a día. Vale aclarar que , además, cada módulo tiene su ejercitación para lograr así una evalauación constante y continua.

### Día 1

1. Html :

- Integración del script en las distintas partes de un código HTML (head y body)

- Visualización de JS : mostrar datos mediante `innerHTML`,`document.write`,`windows.alert`,``console.log`, `document.getElementById`

2. Declaraciones

- ¿Qué es un programa JS?
- Formato y sintáxis de las declaraciones (sangrias, espacios en blanco, punto y coma ,etc)
- Listado de Palabras Claves.

3. Sintaxis

- Comentarios
- Variables: `var`, `let`, `const` y tipo de `datos`.
- Operadores aritméticos: suma, multiplicación, división, módulo, contador, exponenciación

### Día 2

1. Tipo de Datos

- String: comillas, substring, substr, replace, concat, trim, toLowerCase, toUpperCase, split, toStirng y más métodos relacionados al manejo de strings.

- Number: métodos para el manejo de number.

- JS Math: operaciones matemáticas con el objeto JS Math

- Boolean: valores `true` y `false`, función booleana.

### Día 3

1. Introducción a funciones

2. Funciones

- Definiciones
- Parámetros
- Invocaciones
- Call
- Apply 
- Cierres

3. Eventos de js en HTML

4. Declaraciones

- Condicionales
- Cambio
- Bucles

### Día 4

1. Tipos de Datos: Arrays

- Matriz: crear, recorrer, matrices asociativas.

- Elementos: elementos, agregar,cambiar y eliminar.

- Métodos generales: toString, array push, typeOf, length.

2. Ordenación de Arrays

- Sort y Reverse
- Sort numérico
- Comparación
- Random
- Fisher Yates
- Máximos y mínimos 
- Math.max y Math.min
- Ordenar matrices de objetos


3. Iteración de Arrays
=======
### Día 4

1. Iteración de Arrays

- For each
- Find 
- Find Index
- Map 
- Filter 
- Reduce
- Reduce Right
- Every
- Some
- IndexOf
- Last indexOf


=======
2. Introducción a funciones

### Día 5

1. Objetos

- Properties
- Métodos
- Visualización
- Accesores
- Constructores
- Prototipos
- ECMAScript5

### Día 6


=======
1. Funciones

- Definiciones
- Parámetros
- Invocaciones
- Call
- Apply 
- Cierres

2. Clases

- Sintaxis
- Constructor
- Métodos
- Modo Estricto
- Herencia

### Día 7

1. Depuración 

- Console Log
- Debugger

2. Async

- Callback
- Asincrónico
- Promises
- Async-await


=======
3. Eventos de js en HTML

### Día 8

1. Manejo de Errores

- Try/catch
- Objeto Error
- Finally

2. Scope
3. Hoisting

### Dia 9

1. Forms

- Validación Formularios
- Validación Numérica
- API Forms


=======
2. Declaraciones

- Condicionales
- Cambio
- Bucles

### Día 10

1. AJAX

- Introducción
- XMLHttp
- Solicitud
- Respuesta

2. DOM
